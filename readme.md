# AikidoClubBrieComteRobert

AikidoClubBrieComteRobert est un site internet présantant une association d'aïkido à Brie Comte Robert.

## Environnement de développement 

### Pré-requis

* PHP 8.0.11
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante ( de la CLI Symfony) : 

```bash
 symfony check:requirements
```

### Lancer l'environnement de développement

```bash
 docker-compose up -d
 symfony serve -d
```

## Lancer des tests

```bash
php bin/phpunit --testdox
```
